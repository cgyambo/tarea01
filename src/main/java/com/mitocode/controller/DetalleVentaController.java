package com.mitocode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.DetalleVenta;
import com.mitocode.service.IDetalleVentaService;

@RestController
@RequestMapping("/detalleventas")
public class DetalleVentaController {

	@Autowired
	private IDetalleVentaService service;

	@GetMapping
	public List<DetalleVenta> listar() {
		return service.listar();
	}

	@PostMapping
	public DetalleVenta registrar(@RequestBody DetalleVenta dv) {
		return service.registrar(dv);

	}

}
