package com.mitocode.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mitocode.dao.IProductoDAO;
import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;

@Service
public class IProductoServiceImpl implements IProductoService {

	private IProductoDAO dao;

	@Override
	public Producto registrar(Producto prod) {
		return dao.save(prod);
	}

	@Override
	public Producto modificar(Producto prod) {
		return dao.save(prod);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);

	}

	@Override
	public Producto listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

}
