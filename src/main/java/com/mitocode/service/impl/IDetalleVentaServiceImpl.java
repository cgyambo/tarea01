package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IDetalleVentaDAO;
import com.mitocode.model.DetalleVenta;
import com.mitocode.service.IDetalleVentaService;

@Service
public class IDetalleVentaServiceImpl implements IDetalleVentaService {

	@Autowired
	private IDetalleVentaDAO dao;
	@Override
	public DetalleVenta registrar(DetalleVenta dv) {
		
		return dao.save(dv);
	}

	@Override
	public DetalleVenta modificar(DetalleVenta dv) {
		return null;
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DetalleVenta listarId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DetalleVenta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
